public class Sucursal
{
    public string Ubicacion { get; set; }
    public int NumeroSucursal { get; set; }
    public string NumeroTelefonico { get; set; }
    public string Gerente { get; set; }
}