﻿// See https://aka.ms/new-console-template for more information
class Program
{
    static void Main(string[] args)
    {
        Inicio(); // Llamado a menu principal
    }

    static void Inicio()
    {
        while (true)
        {   
            Console.WriteLine("Seleccione una opción:");
            Console.WriteLine("1. Agregar Sucursal");
            Console.WriteLine("2. Consultar");
            Console.WriteLine("3. Salir");

            int opcion = int.Parse(Console.ReadLine());

            switch (opcion) // Escoger opcion de menu
            {
                case 1:
                    AgregarSucursal();
                    break;
                case 2:
                    Consultar();

                    break;
                case 3:
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Opción inválida. Intente nuevamente.");
                    break;
            }
        }
    }
}