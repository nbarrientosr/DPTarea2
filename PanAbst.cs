public abstract class Pan
{
    int cantHarina ;
    int cantAzucar ;
    int cantAgua ;
    int cantLevadura ;
    int cantSal ;


    public string Tipo { get; set; }
    public abstract string ObtenerReceta();
}

// Clases para los tipos específicos de pan
public class PanDulce : Pan
{
    public string Relleno { get; set; }

    public override string ObtenerReceta()
    {
        return $"Tipo de pan: {Tipo}\nIngrediente:\n- Detalle de todos los ingredientes\n- Relleno: {Relleno}\n...";
    }
}

public class PanSalado : Pan
{
    public string Relleno { get; set; }

    public override string ObtenerReceta()
    {
        return $"Tipo de pan: {Tipo}\nIngrediente:\n- Detalle de todos los ingredientes\n- Relleno: {Relleno}\n...";
    }
}

public class Galleta : Pan
{
    public override string ObtenerReceta()
    {
        return $"{Tipo}\nIngrediente:\n- Detalle de todos los ingredientes\n...";
    }
}